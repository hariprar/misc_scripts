import psutil
import time
import resource
import os

def bytes2human(n):
	# http://code.activestate.com/recipes/578019
	# >>> bytes2human(10000)
	# '9.8K'
	# >>> bytes2human(100001221)
	# '95.4M'
	symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
	prefix = {}
	for i, s in enumerate(symbols):
		prefix[s] = 1 << (i + 1) * 10
	for s in reversed(symbols):
		if n >= prefix[s]:
			value = float(n) / prefix[s]
			return '%.1f%s' % (value, s)
	return "%sB" % n


def print_ntuple(nt):
	for name in nt._fields:
		value = getattr(nt, name)
		if name != 'percent':
			value = bytes2human(value)
		print('%-10s : %7s' % (name.capitalize(), value))

def print_disk_partition():
	templ = "%-17s %8s %8s %8s %5s%% %9s  %s"
	print templ % ("Device", "Total", "Used", "Free", "Use ", "Type", "Mount")
	for part in psutil.disk_partitions(all=False):
		if os.name == 'nt':
			if 'cdrom' in part.opts or part.fstype == '':
				# skip cd-rom drives with no disk in it; they may raise
				# ENOENT, pop-up a Windows GUI error for a non-ready
				# partition or just hang.
				continue
		usage = psutil.disk_usage(part.mountpoint)
		print templ % (
			part.device,
			bytes2human(usage.total),
			bytes2human(usage.used),
			bytes2human(usage.free),
			int(usage.percent),
			part.fstype,
			part.mountpoint)

# https://github.com/giampaolo/psutil/blob/master/scripts/meminfo.py
def record_stats():
	while True:
		time.sleep(1)
		print "psutil.cpu_percent:"
		print psutil.cpu_percent()
		print "psutil.cpu_times:" + str(psutil.cpu_times())
		print "psutil.virtual_memory"
		print_ntuple(psutil.virtual_memory())
		print "psutil.disk_usage" + str(psutil.disk_usage('/'))
		print "psutil.disk_io_counters" + str(psutil.disk_io_counters())
		print_disk_partition()
		print "\n"

if __name__ == '__main__':
	record_stats()
	