import os
import subprocess

def cpu_stat():
	f = open("/proc/stat", "r")
	line = f.readline()
	cpu_stat = {}
	while(line):
		llst = line.replace("\n", "").split()

		if llst[0].find("cpu") > -1:
			cpu_stat[llst[0]] = {}
			cpu_stat[llst[0]]["user"] = llst[1]
			cpu_stat[llst[0]]["nice"] = llst[2]	
			cpu_stat[llst[0]]["system"] = llst[3]
			cpu_stat[llst[0]]["idle"] = llst[4]
			cpu_stat[llst[0]]["iowait"] = llst[5]
			cpu_stat[llst[0]]["irq"] = llst[6]
			cpu_stat[llst[0]]["softirq"] = llst[7]

		elif llst[0].find("intr") == -1 and llst[0].find("softirq") == -1:
			cpu_stat[llst[0]] = llst[1]
		line = f.readline()
	f.close()
	return cpu_stat

def mem_info():
	f = open("/proc/meminfo")
	mem_stat = {}
	line = f.readline()
	while line:
		llst = line.replace("\n", "").split(":")
		mem_stat[llst[0].replace(" ", "")] = llst[1].replace(" ", "").replace("kB", "")
		line = f.readline()
	return mem_stat

def disk_stat():
	f = open("/proc/diskstats")
	import re
	return [['Major Number','Minor Number','device number','reads completed','reads merged','sectors read','time spent reading(ms)','writes compeleted','writes merged','sectors written','time spent writing','I/Os  in progress','time spent in doing I/Os','weighted time spent in I/Os']] + [ re.split('\s+',x.strip()) for x in f.readlines() if x.strip()[0]=='8']	

def fs_stat():
	return os.statvfs(".")

def addToCSVFile(fileName,dic,headerAdded):
	import csv
	with open(fileName+'.csv', 'a') as f:  # Just use 'w' mode in 3.x
		w = None
		w = csv.DictWriter(f,sorted(dic.keys()))
		if not headerAdded:
			print 'Writing header'
			w.writeheader()
		w.writerow(dic)

if __name__ == '__main__':
	cpuStat = cpu_stat()
	headerFlag = False
	for stat in cpuStat:
		if type(cpuStat[stat]) == type({}):
			cpuStat[stat]['CPU'] = stat
			addToCSVFile('CPUStats',cpuStat[stat],headerFlag)
			headerFlag = True
	
	memStat = mem_info()
	addToCSVFile('MemStat',memStat,False)
	import csv
	for x in disk_stat():
		print x
	with open('DiskStat.csv','wb') as f:
		w = csv.writer(f)
		w.writerows(disk_stat())
	
