import os
import sys
import json
import tarfile
import numpy as np
import matplotlib.pyplot as plt

from ProcessStats import ProcessStats

class PlotStats():
	def __init__(self, data_path, param_config_path, test_ids, metric):
		self.path = data_path
		self.ids = test_ids
		self.metric = metric
		self.param_path = param_config_path


	def process_json(self, data):
		xy_data_dict = {}
		for ts in data:
			# xaxis.append(ts)
			for stats_type in data[ts]:
				stat_content = data[ts][stats_type]
				if stats_type == "mem_info":
					stat_content["MemUsed"] = int(stat_content["MemTotal"]) - int(stat_content["MemFree"])
					stat_content["SwapUsed"] = int(stat_content["SwapTotal"]) - int(stat_content["SwapFree"])

				if stats_type == "disk_stat":
					stat_content = data[ts][stats_type][0]

				if stats_type == "cpu_stat":
					if not self.metric in stat_content:
						stat_content = stat_content["cpu"]

				if self.metric in stat_content:
					# yaxis.append(stat_content[self.metric])
					xy_data_dict[int(ts)] = float(stat_content[self.metric])/self.param_json[self.metric]["factor"]
		xaxis = []
		yaxis = []
		for key in sorted(xy_data_dict.keys()):
			xaxis.append(key)
			yaxis.append(xy_data_dict[key])
		# print xaxis
		# print yaxis
		return xaxis, yaxis

	def draw_plot(self, data):
		fig = plt.figure()
		ax = plt.subplot(111)
		ax.set_xlabel('Time (secs)')
		ax.set_ylabel(self.metric+ ' (' + self.param_json[self.metric]["units"] + ')')
		ax.set_title('Time vs ' + self.metric + '\n' + self.param_json[self.metric]["description"])
		ax.grid(True)
		xdata = []
		ydata = []
		plot_legend = []
		min_y = sys.maxint
		for line in data:
			min_x = min([int(x) for x in data[line]["x"]])
			xdata.append([int(x) - int(min_x) for x in data[line]["x"]])
			# ydata.append([int(y) - int(min_y) for y in data[line]["y"]])
			ydata.append([float(y) for y in data[line]["y"]])
			min_y = min(min(ydata[-1]), min_y)
			ax.plot(xdata[-1], ydata[-1], marker='^', markerfacecolor='None')
			plot_legend.append(line + "<throughput>")

		# red dashes, blue squares and green triangles
		# plt.plot(xdata[0], ydata[0], 'ro', xdata[1], ydata[1], 'bs', xdata[2], ydata[2], 'g^')

		# for i in xrange(len(ydata)):
		# 	ydata[i] = [y - min_y for y in ydata[i]]
		# 	ax.plot(xdata[i], ydata[i], marker='^', markerfacecolor='None')

		# Shrink current axis by 20%
		box = ax.get_position()
		ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
		ax.set_ylim(ymin=0)

		# Put a legend to the right of the current axis
		ax.legend(plot_legend, loc='center left', bbox_to_anchor=(1, 0.5))

		# ax.text(0.5, 0.9,"Minimum y = " + str(min_y) + " " + self.param_json[self.metric]["units"], horizontalalignment='center', verticalalignment='center', transform=ax.transAxes)
		plt.show()

	def load_param_json(self):
		with open(self.param_path, "r") as f:
			self.param_json = json.loads(f.read())

	def plot(self):
		self.load_param_json()
		updateCWD(self.path)
		plot_data = {}
		for id in self.ids:
			ob = json.load(open(id+".json"))
			plot_data[id] = {}
			plot_data[id]["x"], plot_data[id]["y"] = self.process_json(ob)
		self.draw_plot(plot_data)

def stat_folder(members):
	root_path = None
	for tarinfo in members:
		# Spliting the path to extract last subdir
		if os.path.split(tarinfo.name)[1] == "stats":
			root_path = tarinfo.name
		if root_path is not None:
			if tarinfo.name.startswith(root_path):
				yield tarinfo

def untar(fname, out_path):
	tar = tarfile.open(fname)
	tar.extractall(path=out_path, members=stat_folder(tar))
	tar.close()

def updateCWD(path):
	if os.path.exists(path):
		# print "updating wd to", path
		os.chdir(path)

if __name__ == "__main__":
	suffix = "_output.tar.bz2"
	data_path = os.path.join(os.getcwd(), "output")
	untar_path = os.path.join(os.getcwd(), "untar")
	json_path = os.path.join(os.getcwd(), "jsons")
	param_config_path = os.path.join(os.getcwd(), "param_config.json")
	ids = ["322386", "322387", "322388", "322389", "322390"]
	create_json = False
	# ids = ["322386", "322387", "322388", "322389", "322390"]
	# print os.getcwd()
	# print os.getcwd()
	if create_json:
		for test_id in ids:
			updateCWD(data_path)
			# print "PlotStats", data_path
			untar(test_id+suffix, untar_path)
			p = ProcessStats(test_id, "sde1", untar_path)
			p.process()
	pl = PlotStats(json_path, param_config_path, ids, "MemFree")
	pl.plot()

