import os
import subprocess
from threading import Thread
import time
import json
import atexit

class Stats():
	def __init__(self, config_name, output_fname, fs_path):
		self.config_name = config_name
		self.output_fname = output_fname
		# self.run_time = run_time
		self.fs_path = fs_path
		self.collecting = False
		self.interval = 5
		self.thread = Thread(target = self.collect)
		self.thread.daemon = True
		atexit.register(self.on_exit)

	def set_interval(self, seconds):
		self.interval = seconds

	def on_exit(self):
		print "Stopping stat collection on exit"
		self.stop_stat_collection()

	def start_stat_collection(self):
		self.collecting = True
		print "Attempting to start thread"
		print "Starting thread to collect stats as a daemon"
		self.thread.start()

	def stop_stat_collection(self):
		self.collecting = False
		print "Attempting to stop thread"
		self.thread.join()

	def collect(self, should_autocollect=False):
		print "Thread started! Collecting stats for config: ", self.config_name
		obj = {}
		obj[self.config_name] = {}

		# if should_autocollect:
		# 	count = self.run_time/self.interval
		# else:
		# 	count = 1

		while 1:
			epoch_time = str(int(time.time()))
			obj[self.config_name][epoch_time] = {}
			obj[self.config_name][epoch_time]["cpu_stat"] = self.cpu_stat()
			obj[self.config_name][epoch_time]["mem_info"] = self.mem_info()
			obj[self.config_name][epoch_time]["disk_stat"] = self.disk_stat()
			obj[self.config_name][epoch_time]["fs_stat"] = self.fs_stat()
			# count -= 1
			# if count == 0:
			# 	break
			time.sleep(self.interval)
			if not self.collecting:
				print "Stats collection stopped"
				break

		# TODO: Check if the file already exists
		# TODO: Put these output files to a specific folder
		with open(self.output_fname, "w") as f:
			f.write(json.dumps(obj))
		print "Wrote collected stats to ", self.output_fname
		print "Terminating thread"
		return

	def cpu_stat(self):
		f = open("/proc/stat", "r")
		line = f.readline()
		cpu_stat = {}
		while(line):
			llst = line.replace("\n", "").split()

			if llst[0].find("cpu") > -1:
				cpu_stat[llst[0]] = {}
				cpu_stat[llst[0]]["user"] = llst[1]
				cpu_stat[llst[0]]["nice"] = llst[2]
				cpu_stat[llst[0]]["system"] = llst[3]
				cpu_stat[llst[0]]["idle"] = llst[4]
				cpu_stat[llst[0]]["iowait"] = llst[5]
				cpu_stat[llst[0]]["irq"] = llst[6]
				cpu_stat[llst[0]]["softirq"] = llst[7]

			elif llst[0].find("intr") == -1 and llst[0].find("softirq") == -1:
				cpu_stat[llst[0]] = llst[1]
			line = f.readline()
		f.close()
		print "Collected CPU stats for config:", self.config_name
		return cpu_stat

	def mem_info(self):
		f = open("/proc/meminfo")
		mem_stat = {}
		line = f.readline()
		while line:
			llst = line.replace("\n", "").split(":")
			mem_stat[llst[0].replace(" ", "")] = llst[1].replace(" ", "").replace("kB", "")
			line = f.readline()
		print "Collected memory info stats for config:", self.config_name
		return mem_stat

	def disk_stat(self):
		f = open("/proc/diskstats")
		headers = ['major_num','minor_num','device_num','reads_completed', \
				   'reads_merged','sectors_read','time_spent_reading', \
				   'writes_compeleted','writes_merged','sectors_written', \
				   'time_spent_writing','IOs_in_progress', \
				   'time_spent_doing_IOs','weighted_time_spent_IOs']
		disk_stat = []
		line = f.readline()
		while line:
			llst = line.replace("\n", "").split()
			if llst[0] == '8' and self.fs_path == llst[2]:
				print llst
				obj = {}
				for i in xrange(len(headers)):
					obj[headers[i]] = llst[i]
				disk_stat.append(obj)
			line = f.readline()
		print "Collected disk stats for fs %s for config: %s" % (self.fs_path self.config_name)
		return disk_stat

	'''
	f_bsize: preferred file system block size.
	f_frsize: fundamental file system block size.
	f_blocks: total number of blocks in the filesystem.
	f_bfree: total number of free blocks.
	f_bavail: free blocks available to non-super user.
	f_files: total number of file nodes.
	f_ffree: total number of free file nodes.
	f_favail: free nodes available to non-super user.
	f_flag: system dependent.
	f_namemax: maximum file name length.
	'''
	def fs_stat(self, path = "."):
		path = "/dev/"+self.fs_path
		statvfs = os.statvfs(path)
		fs_st = {}
		fs_st["f_bsize"] = statvfs.f_bsize
		fs_st["f_frsize"] = statvfs.f_frsize
		fs_st["f_blocks"] = statvfs.f_blocks
		fs_st["f_bfree"] = statvfs.f_bfree
		fs_st["f_bavail"] = statvfs.f_bavail
		fs_st["f_files"] = statvfs.f_files
		fs_st["f_ffree"] = statvfs.f_ffree
		fs_st["f_favail"] = statvfs.f_favail
		fs_st["f_flag"] = statvfs.f_flag
		fs_st["f_namemax"] = statvfs.f_namemax
		print "Collected fs stat for the fs mounted in path %s for config: %s" % (path, self.config_name)
		return fs_st

# if __name__ == '__main__':
# 	s = Stats("config1", "config1.json", "sde1")
# 	s.start_stat_collection()
# 	time.sleep(30) # Or execute filebench
# 	s.stop_stat_collection()
