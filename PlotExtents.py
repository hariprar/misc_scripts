import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.patches import Rectangle

class PlotExtents():
	def __init__(self, fname):
		self.fname = fname
	def process_data(self):
		total_extents = 0
		extent_len = []
		physical_len = []
		with open(self.fname, "r") as f:
			line = f.readline()
			line = f.readline()
			line = f.readline()
			while line:
				lline = line.split()
				p = []
				p.append(int(lline[7]))
				p.append(int(lline[9]))
				total_extents = int(lline[3])
				extent_len.append(int(lline[-1]))
				physical_len.append(p)
				# print lline
				# print line
				line = f.readline()
				# break
		# print "total_extents = ", total_extents
		# print extent_len
		# print physical_len
		return physical_len

	def draw_plot(self):
		x = self.process_data()
		x_start = [i[0] for i in x]
		x_end = [i[1] for i in x]

		min_x = min(x_start)
		x_start = [i-min_x for i in x_start]
		x_end = [i-min_x for i in x_end]

		y_start = [i+1 for i in xrange(0, len(x))]
		y_end = [i+1 for i in xrange(0, len(x))]
		X = []
		Y = []
		X.append(x_start)
		X.append(x_end)
		Y.append(y_start)
		Y.append(y_end)
		plt.plot(X, Y,"k--*")
		plt.xlabel('Physical length')
		plt.ylabel('Entries')
		leg1 = Rectangle((0, 0), 0, 0, alpha=0.0)
		line = []
		line.append("x-min:%s" %min_x)

		plt.legend([leg1],line, handlelength=0)
		plt.show()


if __name__ == "__main__":
	pe = PlotExtents("dump2")
	pe.draw_plot()
