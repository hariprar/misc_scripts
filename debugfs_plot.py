import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.patches import Rectangle

class PlotExtents():
	def __init__(self, fname):
		self.fname = fname
	def process_data(self):
		total_extents = 0
		extent_len = []
		physical_len = []
		with open(self.fname, "r") as f:
			line = f.readline()
			line = f.readline()
			while line:
				p = []
				lline = line.split(':')
				phylen = lline[2].split('..')
				p.append(int(phylen[0]))
				p.append(int(phylen[1]))
				physical_len.append(p)
				line = f.readline()	
		return physical_len

	def draw_plot(self):
		x = self.process_data()

		x_start = [i[0] for i in x]
		x_end = [i[1] for i in x]

		min_x = min(x_start)
		x_start = [i-min_x for i in x_start]
		x_end = [i-min_x for i in x_end]
		
		y_start = [i+1 for i in xrange(0, len(x))]
		y_end = [i+1 for i in xrange(0, len(x))]
		X = []
		Y = []
		X.append(x_start)
		X.append(x_end)
		Y.append(y_start)
		Y.append(y_end)
		plt.plot(X, Y,"k--*")
		plt.xlabel('Physical Offset')
		plt.ylabel('Entries')
		leg1 = Rectangle((0, 0), 0, 0, alpha=0.0)
		line = []
		line.append("x-min:%s" %min_x)
		plt.legend([leg1],line, handlelength=0)
		plt.show()


if __name__ == "__main__":
	pe = PlotExtents("dump1")
	pe.draw_plot()
