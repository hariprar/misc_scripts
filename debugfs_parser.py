import os
import cPickle as pkl
from cycler import cycler
import matplotlib.pyplot as plt
import math

# DATA_FOLDER = 'result_gr5'
THROUGHPUT = 'throughput.txt'
#
# CONTENT_FILE = DATA_FOLDER+ '_extract.pkl'


def read_file(fname, m):
    print 'Parsing file: {}'.format(fname)
    # Remove .txt and take the last item after spliting the path
    key = os.path.split(os.path.splitext(fname)[0])[-1]
    with open(fname, 'r') as f:
        line = f.readline()
        x = []
        while line:
            line = f.readline()
            line = f.readline()
            x.append(line.split()[7])
            line = f.readline()
            line = f.readline()
    m[key] = x
    print 'Adding data points to `m` with key = {}'.format(key)
    return m

def transform_x(x, flag=False):
    x = float(x)
    # if flag:
    #     return '(throughtput/10)^2'
    # return math.log(x)
    return x

def transform_y(y, flag=False):
    y = float(y)
    if flag:
        return '(throughtput/10)^2'
    return (y/10)**2


def plot_data(data, throughput):
    # fig = plt.figure()
    for key in data:
        ax = plt.subplot(111)
        ax.set_xlabel('Physical Block Number (block size 2048)')
        ax.set_ylabel('Throughput')
        ax.set_title('Throughput vs Physical Block Number (block size 2048)')
        ax.grid(True)
        # Changing the color pallete to Red-Yellow-Green
        cm = plt.get_cmap('RdYlGn')

        # Assuming all result folders have same number of test outputs
        # NUM_COLORS = len(data[data.keys()[0]]) * len(data)
        NUM_COLORS = len(throughput[key])
        colors = [cm(1.0*(i)/NUM_COLORS) for i in xrange(1,NUM_COLORS+1)]
        # Reverse the colors
        # To assign higher throughputs GREEN and lower throughputs RED
        ax.set_prop_cycle(cycler('color', colors[::-1]))
        xdata = []
        ydata = []
        plot_legend = []

        m = data[key]
        print m.keys()
        t = sorted(throughput[key].items(), key=lambda k: k[1], reverse=True)
        y_min = 0
        y_min = min([x[1] for x in t])
        y_min = (int(float(y_min))/100)*100
        # y_max = max([x[1] for x in t])
        # y_max = (int(float(y_max)+100)/100)*100
        limit = -1

        for i, k in enumerate(t):
            # print k
            # Constructing points to be plotted
            xdata = [transform_x(x) for x in m[k[0]][:limit]]
            # print min(xdata), max(xdata)
            ydata = [transform_y(k[1])] * len(m[k[0]][:limit])
            marker = 'o'
            if key == "output_old":
                marker = 'go'
            ax.plot(xdata, ydata, marker)  # marker='s', markerfacecolor='None')
            plot_legend.append("{0}_{1} = {2} [{3}]".format(key, k[0], k[1], transform_y(k[1])))

        # Shrink current axis by 20%
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        ax.set_ylim(ymin=transform_y(y_min))
        # ax.set_ylim(ymax=y_max)
        ax.set_xlim(xmin=0)
        # ax.set_xlim(xmax=9000000)

        # Put a legend to the right of the current axis
        ax.legend(plot_legend,
                    title='Throughput transformation -> '+transform_y(1, True),
                    numpoints=1,
                    loc='center left',
                    bbox_to_anchor=(1, 0.5),
                    prop={'size':10})

        ax.relim()
        ax.autoscale_view(True,True,True)
        figure = plt.gcf()
        figure.set_size_inches(19.2, 10.8)
        plt.savefig("plot-{}.png".format(key), dpi = 100)
        print 'Saved figure - plot-{}.png'.format(key)

        # Shrink current axis by 20%
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        ax.set_ylim(ymin=0)
        # ax.set_ylim(ymax=y_max)
        ax.set_xlim(xmin=0)
        # ax.set_xlim(xmax=9000000)

        # Put a legend to the right of the current axis
        ax.legend(plot_legend,
                    title='Throughput transformation -> '+transform_y(1, True),
                    numpoints=1,
                    loc='center left',
                    bbox_to_anchor=(1, 0.5),
                    prop={'size':10})

        ax.relim()
        ax.autoscale_view(True,True,True)
        figure = plt.gcf()
        figure.set_size_inches(19.2, 10.8)
        plt.savefig("plot-{}-0.png".format(key), dpi = 100)
        print 'Saved figure - plot-{}-0.png'.format(key)

        plt.clf()


def main():
    data = dict()
    throughput = dict()
    # folders = ['20G_1', '160G_1', '40G_1']
    folders = ['20G_1']
    for folder in folders:
        path = os.path.join(os.getcwd(), folder)

        with open(os.path.join(path, THROUGHPUT), 'r') as f:
            lines = [line.split()[0].split(',') for line in f.readlines()]
            throughput[folder] = dict(lines)

        if not os.path.exists(folder+'_extract.pkl'):
            m = dict()
            for file_name in os.listdir(path):
                if file_name.find('throughput') > -1:
                    continue
                m = read_file(os.path.join(path, file_name), m)

            with open(folder+'_extract.pkl', 'w') as f:
                pkl.dump(m, f)

        with open(folder+'_extract.pkl', 'r') as f:
            data[folder] = pkl.load(f)

    plot_data(data, throughput)


if __name__ == '__main__':
    main()
