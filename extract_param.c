	def extract_param(self, stat, param):
		ifile = open('tmp0','r')
		ofile = open('tmp2','w')
		ofilep = open('t.dat','w')
		json_parameters = json.load(ifile)
		
		timestamps = json_parameters[self.config_name]

		param_dict = {}
		coords = [] 
		for timestamp in timestamps:
			ofile.write(str(timestamp) + str("\n"))
			t = timestamps[timestamp]
			stat_obj = t[stat]
			p = stat_obj[param]	
			param_dict[timestamp] = p

		x = []
		y = []
		for item in param_dict:
			x.append(item)
			y.append((param_dict[item]))
			ofilep.write(str(item) + str(",") + str(param_dict[item]) + str("\n"))        
			
       
		min_x_val = min(x)
		max_x_val = max(x)

		min_y_val = min(y)
		max_y_val = max(y)

		#set yrange [min_y_val,max_y_val]
		gp = Gnuplot.Gnuplot(persist=1)
		gp('set datafile separator ","')
		gp('set grid')

		databuff = Gnuplot.File("t.dat")
		gp.plot(databuff)
		return 

