import os
import subprocess
from threading import Thread
import time
import json
import atexit

class ProcessStats():
	def __init__(self, test_id, fs_path, data_dir):
		self.id = test_id
		self.fs_path = fs_path
		self.data_dir = data_dir
		self.json_path = ""

	def checkAndCreateDir(self, path, alert=False):
		if not os.path.exists(path):
			sp.call(['mkdir', path])
		else:
			if alert:
				print "Folder %s cannot exist already. Exiting..." % (path)
				sys.exit()

	def updateCWD(self, path):
		if os.path.exists(path):
			# print "updating wd to", path
			os.chdir(path)

	def process(self):
		print "Processing data from: ", self.data_dir
		# print "ID : ", self.id
		# print "ProcessStats", os.getcwd()
		obj = {}
		flag = False
		for root, dirs, files in os.walk(self.data_dir):
			for dirname in dirs:
				if dirname.find(self.id) > -1:
					self.updateCWD(os.path.join(root, dirname))
					flag = True
					# print os.getcwd()

		stat_folder = os.path.join(os.getcwd(), "stats")
		# print "Finding stat files in folder : ", stat_folder

		# If processed json needs to stored in separate folder, update
		# output_fname with absolute path of folder name
		output_fname = None
		for subdirname in os.listdir(stat_folder):
				self.json_path = os.path.join(self.data_dir, "../jsons/")
				output_fname = self.json_path+self.id+".json"
				print "output_fname", output_fname
				path = os.path.join(stat_folder, subdirname)
				# print "Path", path
				obj[subdirname] = {}
				obj[subdirname]["cpu_stat"] = self.cpu_stat(path)
				obj[subdirname]["mem_info"] = self.mem_info(path)
				obj[subdirname]["disk_stat"] = self.disk_stat(path)
				obj[subdirname]["fs_stat"] = self.fs_stat(path)
				print ""

		if not flag:
			print "Raw stats data for test id : %s not found in given " \
						"output directory: %s" % (self.id, self.data_dir)
			return
		with open(output_fname, "w") as f:
			f.write(json.dumps(obj))
		print "Wrote collected stats to ", output_fname

	def cpu_stat(self, path):
		cpu_stat = {}
		with open(os.path.join(path, "cpuinfo"), 'r') as f:
			line = f.readline()
			while(line):
				llst = line.replace("\n", "").split()
				if llst[0].find("cpu") > -1:
					cpu_stat[llst[0]] = {}
					cpu_stat[llst[0]]["user"] = llst[1]
					cpu_stat[llst[0]]["nice"] = llst[2]
					cpu_stat[llst[0]]["system"] = llst[3]
					cpu_stat[llst[0]]["idle"] = llst[4]
					cpu_stat[llst[0]]["iowait"] = llst[5]
					cpu_stat[llst[0]]["irq"] = llst[6]
					cpu_stat[llst[0]]["softirq"] = llst[7]

				elif llst[0].find("intr") == -1 and llst[0].find("softirq") == -1:
					cpu_stat[llst[0]] = llst[1]
				line = f.readline()
		print "Collected CPU stats for config:", self.id
		return cpu_stat

	def mem_info(self, path):
		with open(os.path.join(path, "meminfo"), 'r') as f:
			mem_stat = {}
			line = f.readline()
			while line:
				llst = line.replace("\n", "").split(":")
				mem_stat[llst[0].replace(" ", "")] = llst[1].replace(" ", "").replace("kB", "")
				line = f.readline()
		print "Collected memory info stats for config:", self.id
		return mem_stat

	def disk_stat(self, path):
		headers = ['major_num','minor_num','device_num','reads_completed', \
				   'reads_merged','sectors_read','time_spent_reading', \
				   'writes_compeleted','writes_merged','sectors_written', \
				   'time_spent_writing','IOs_in_progress', \
				   'time_spent_doing_IOs','weighted_time_spent_IOs']
		disk_stat = []
		with open(os.path.join(path, "diskstats"), 'r') as f:
			line = f.readline()
			while line:
				llst = line.replace("\n", "").split()
				if llst[0] == '8' and self.fs_path == llst[2]:
					# print llst
					obj = {}
					for i in xrange(len(headers)):
						obj[headers[i]] = llst[i]
					disk_stat.append(obj)
				line = f.readline()
		print "Collected disk stats for fs %s for config: %s" % (self.fs_path, self.id)
		return disk_stat

	'''
	f_bsize: preferred file system block size.
	f_frsize: fundamental file system block size.
	f_blocks: total number of blocks in the filesystem.
	f_bfree: total number of free blocks.
	f_bavail: free blocks available to non-super user.
	f_files: total number of file nodes.
	f_ffree: total number of free file nodes.
	f_favail: free nodes available to non-super user.
	f_flag: system dependent.
	f_namemax: maximum file name length.
	'''
	def fs_stat(self, path):
		with open(os.path.join(path, "fsstats"), 'r') as f:
			content = json.loads(f.read())
		print "Collected filesystem stats for config:", self.id
		return content

	def getJsonPath(self):
		return self.json_path

if __name__ == '__main__':
	p = ProcessStats("4515", "sda1", "/root/output")
	p.process()
