import os
import sys
import time
import json
import atexit
import tarfile
import subprocess as sp
from ProcessStats import ProcessStats

from threading import Thread

class StoreStats():
	def __init__(self, test_id, fs_path, out_path):
		self.id = test_id
		self.fs_path = fs_path
		self.out_path = out_path
		self.interval = 5
		self.thread = Thread(target = self.store)
		self.thread.daemon = True
		self.running = False
		atexit.register(self.onExit)

	def setInterval(self, new_seconds):
		self.interval = new_seconds

	def onExit(self):
		print "Stopping stat collection on exit"
		self.stop()

	def checkAndCreateDir(self, path, alert=False):
		if not os.path.exists(path):
			sp.call(['mkdir', path])
		else:
			if alert:
				print "Folder %s cannot exist already. Exiting..." % (path)
				sys.exit()

	def updateCWD(self, path):
		if os.path.exists(path):
			# print "updating wd to", path
			os.chdir(path)

	def start(self):
		self.checkAndCreateDir(self.out_path)
		# if os.path.isdir(self.out_path):
		# 	print self.out_path, "is directory"
		self.updateCWD(self.out_path)

		# print os.getcwd()
		self.checkAndCreateDir(self.id, alert=True)
		self.updateCWD(self.id)
		self.stats_data_path = os.getcwd()
		# print "before creating test_id folder"
		# print os.getcwd()
		# print "Output path = ", os.getcwd()
		# print "Disinfecting output path..."
		print "Starting thread"
		self.thread.start()
		self.running = True

	def store(self):
		print "Begin collecting stats"
		while 1:
			epoch_time = str(int(time.time()))
			self.checkAndCreateDir(epoch_time)
			self.updateCWD(epoch_time)

			sp.call(['cp', '/proc/stat', 'cpuinfo'])
			sp.call(['cp', '/proc/meminfo', 'meminfo'])
			sp.call(['cp', '/proc/diskstats', 'diskstats'])
			with open("fsstats", "w") as f:
				f.write(json.dumps(self.fs_stat()))
			self.updateCWD(self.stats_data_path)
			time.sleep(self.interval)
			if not self.running:
				break
		print "Creating tarball of collected stats"
		self.make_tarfile()
		print "Exiting store function"

	def make_tarfile(self):
		output_filename = self.id+".tar.bz2"
		source_dir = self.stats_data_path
		print "output_filename", output_filename
		print "source_dir", source_dir
		self.updateCWD(self.out_path)
		with tarfile.open(output_filename, "w:bz2") as tar:
			tar.add(source_dir, arcname=os.path.basename(source_dir))

	def fs_stat(self, path = "."):
		path = "/dev/"+self.fs_path
		statvfs = os.statvfs(path)
		fs_st = {}
		fs_st["f_bsize"] = statvfs.f_bsize
		fs_st["f_frsize"] = statvfs.f_frsize
		fs_st["f_blocks"] = statvfs.f_blocks
		fs_st["f_bfree"] = statvfs.f_bfree
		fs_st["f_bavail"] = statvfs.f_bavail
		fs_st["f_files"] = statvfs.f_files
		fs_st["f_ffree"] = statvfs.f_ffree
		fs_st["f_favail"] = statvfs.f_favail
		fs_st["f_flag"] = statvfs.f_flag
		fs_st["f_namemax"] = statvfs.f_namemax
		print "Collected fs stat for the fs mounted in path %s for " \
				"config: %s" % (path, self.id)
		return fs_st

	def stop(self):
		self.running = False
		print "Attempting to stop the thread"
		if self.thread.isAlive():
			self.thread.join()
		print "Thread exited successfully"

	def getOutputPath(self):
		# return self.stats_data_path
		return self.out_path

	def getFsPath(self):
		return self.fs_path

	def getTestId(self):
		return self.id

if __name__ == '__main__':
	test_id = "4515"
	disk = "sda1"
	outpath = "/root/output"
	s = StoreStats(test_id, disk, outpath)
	s.start()
	time.sleep(6)
	s.stop()
	print s.getOutputPath()
	print s.getFsPath()
	print s.getTestId()
	p = ProcessStats(test_id, disk, outpath)
	p.process()

